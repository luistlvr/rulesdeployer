package br.pucrio.inf.lac.models.base;

import java.io.Serializable;

import com.google.gson.JsonIOException;

/**
 * Created by Luis on 13/05/15.
 * Query representation of an external message
 */
public abstract class QueryMessage implements Serializable {
	/** UID */
	private static final long serialVersionUID = -5407635658836169711L;
	
	/** Mobile Hub available services */
	public enum SERVICE {
        MEPA( "MEPA" ),
        S2PA( "S2PA" );
		
		private String value;
		public static final int length = values().length;
		
		SERVICE( String value ) {
			this.value = value;
		}
		
		@Override 
		public String toString() {
		    return value;
		}
		
		public static SERVICE fromString( String text ) {
            if( text != null ) {
                for( SERVICE b : SERVICE.values() ) {
                    if( text.equalsIgnoreCase( b.value ) )
                        return b;
                }
            }
            throw new IllegalArgumentException( "No constant with text " + text + " found" );
        }
	}
	
	/** Available types of query */
	public enum ACTION {
		ADD   ( "add" ),
        REMOVE( "remove" ),
        START ( "start" ),
        STOP  ( "stop" ),
        CLEAR ( "clear" ),
        GET   ( "get" );
		
		private String value;
		
		public static final int length = values().length;
		
		ACTION( String value ) {
			this.value = value;
		}
		
		@Override 
		public String toString() {
		    return value;
		}
		
		public static ACTION fromString(String text) {
            if( text != null ) {
                for( ACTION b : ACTION.values() ) {
                    if( text.equalsIgnoreCase( b.value ) )
                        return b;
                }
            }
            throw new IllegalArgumentException( "No constant with text " + text + " found" );
        }
	}
	
	/** Types of objects */
    public enum ITEM {
        RULE     ( "rule" ),
        EVENT    ( "event" ),
        // Only local
        LOCATION ( "location" );

        private String value;

        ITEM( String value ) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public static ITEM fromString(String text) {
            if( text != null ) {
                for( ITEM b : ITEM.values() ) {
                    if( text.equalsIgnoreCase( b.value ) )
                        return b;
                }
            }
            throw new IllegalArgumentException( "No constant with text " + text + " found" );
        }
    }
    
    /** Types of targets */
    public enum ROUTE {
        LOCAL  ( "local" ),
        GLOBAL ( "global" );

        private String value;

        ROUTE( String value ) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public static ROUTE fromString( String text ) throws IllegalArgumentException {
            if( text != null ) {
                for( ROUTE b : ROUTE.values() ) {
                    if( text.equalsIgnoreCase( b.value ) )
                        return b;
                }
            }
            throw new IllegalArgumentException( "No constant with text " + text + " found" );
        }
    }
    
    /** Types of data */
    public enum DATUM {
        STRING ( "string" ),
        DOUBLE ( "double" );

        private String value;

        DATUM( String value ) {
            this.value = value;
        }

        @Override
        public String toString() {
            return value;
        }

        public static DATUM fromString( String text ) throws IllegalArgumentException {
            if( text != null ) {
                for( DATUM b : DATUM.values() ) {
                    if( text.equalsIgnoreCase( b.value ) )
                        return b;
                }
            }
            throw new IllegalArgumentException( "No constant with text " + text + " found" );
        }
    }
	
	/** JSON Keys */
	protected static final String TYPE   = "type";   // e.g. "add", "remove", "get", or "clear"
	protected static final String OBJECT = "object"; // e.g. "rule", "event"
	protected static final String LABEL  = "label";  // e.g. "HighTemperature"
	protected static final String RULE   = "rule";   // e.g. [ "SELECT * FROM SensorData;", ... ]
	protected static final String EVENT  = "event";  // e.g. [ [ "var1", "double" ], [ "var2", "string" ] ]
	protected static final String DEVICE = "device"; // [ "1-bc:6a:29:ac:68:04", ... ]
	protected static final String TARGET = "target"; // "white", "black" (2PA) "local", "global" (MEPA)
	
	/** Priority options */
    protected static final int HIGH   = 2;
    protected static final int MEDIUM = 1;
    protected static final int LOW    = 0;
	
	/**
	 * Transform the object to a JSON structure
	 * @return The JSON String
	 */
	public abstract String toJSON() throws JsonIOException;
}
