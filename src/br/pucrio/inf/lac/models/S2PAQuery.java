package br.pucrio.inf.lac.models;

import com.google.gson.JsonIOException;

import br.pucrio.inf.lac.models.base.QueryMessage;
import br.pucrio.inf.lac.models.base.QueryMessage.ACTION;

/**
 * Created by luis on 2/07/15.
 * POJO that represents a query message
 * to the S2PA service
 */
public class S2PAQuery extends QueryMessage {
	private static final long serialVersionUID = -1512755301705515673L;

	/** DEBUG */
    public static final String TAG = "S2PAQuery";
    
    private ACTION type;
	
	@Override
	public String toJSON() throws JsonIOException {
		// TODO Auto-generated method stub
		return null;
	}

}
