package br.pucrio.inf.lac.models;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;

import br.pucrio.inf.lac.models.base.QueryMessage;

import com.google.gson.JsonIOException;

/**
 * Created by Luis on 1/07/15.
 * POJO that represents a query message
 * to the MEPA service
 */
public class MEPAQuery extends QueryMessage {
	private static final long serialVersionUID = 6935673807741894966L;

	/** DEBUG */
    public static final String TAG = "MEPAQuery";

    private ACTION type;
    private ITEM object;
    private ROUTE target;
    private String label;
    private String rule;
    private Map<String, Object> event = new HashMap<>();

    /** Getters */
    public ACTION getType() {
        return this.type;
    }
    
    public ITEM getObject() {
        return this.object;
    }
    
    public ROUTE getTarget() {
        return this.target;
    }

    public String getLabel() {
        return this.label;
    }

    public String getRule() {
        return this.rule;
    }
    /** Getters */
    
    /** Setters */
	public void setType( ACTION type ) {
		this.type = type;
	}
	
	public void setObject( ITEM object ) {
		this.object = object;
	}
	
	public void setTarget( ROUTE target ) {
		this.target = target;
	}
	
	public void setLabel( String label ) {
		this.label = label;
	}
	
	public void setRule( String rule ) {
		this.rule = rule;
	}
	
	public void setEvent( Map<String, Object> event ) {
		this.event = event;
	}
	/** Setters */
    
    @SuppressWarnings("unchecked")
	@Override
	public String toJSON() {
		JSONObject query = new JSONObject();
		JSONObject data  = new JSONObject();
		
		try {
			data.put( TYPE,  type.toString() );
			
			if( object != null )
				data.put( OBJECT, object.toString() );
			
			if( target != null )
				data.put( TARGET, target.toString() );
			
			if( label != null )
				data.put( LABEL, label );
			
			if( rule != null )
				data.put( RULE, rule );	
			
			query.put( TAG, data );
		} catch(JsonIOException ex) {
			System.out.println( ex.getMessage() );
		}
		
		return query.toString();
	}

    @Override
    public String toString() {
        return TAG + " [type=" + type + ", label=" + label
                + ", rule=" + rule + "]";
    }
}
