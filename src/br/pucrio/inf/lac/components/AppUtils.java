package br.pucrio.inf.lac.components;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class AppUtils {
	private static final Random rand = new Random();
	/**
	 * Returns a pseudo-random number between min and max, inclusive.
	 * The difference between min and max can be at most
	 * <code>Integer.MAX_VALUE - 1</code>.
	 *
	 * @param min Minimum value
	 * @param max Maximum value.  Must be greater than min.
	 * @return Integer between min and max, inclusive.
	 * @see java.util.Random#nextInt(int)
	 */
	public static int randInt(int min, int max) {
	    int randomNum = rand.nextInt( (max - min) + 1 ) + min;
	    return randomNum;
	}
	
	/**
	 * Prints data to DEBUG
	 * @param TAG (String) The text to print before the message
	 * @param text (String) The message (e.g. An error)
	 */
	public static void Logger(String TAG, String text) {
		if( AppConfig.DEBUG )
			System.out.println( ">> " + TAG + ": " + text );
	}
	
	/**
	 * A simple check to see if a string is a valid number before inserting
	 * into the shared preferences.
	 * 
	 * @param s The number to be checked.
	 * @return true  It is a number.
	 *         false It is not a number.
	 */
	public static Boolean isNumber(String s) {
		try {
            Integer.parseInt( s );
        }
		catch(NumberFormatException e) {
			return false;			
		}
		return true;
	}
	
	/**
	 * 
	 * @param fileName
	 * @return
	 * @throws FileNotFoundException
	 * @throws URISyntaxException
	 */
	public static String readFile(String fileName) throws FileNotFoundException, URISyntaxException {
		File file = new File( AppUtils.class.getResource( fileName ).toURI() );
		StringBuilder fileContents = new StringBuilder( (int)file.length() );
	    Scanner scanner = new Scanner( file );
	    String lineSeparator = System.getProperty( "line.separator" );
	
	    try {
	        while( scanner.hasNextLine() )
	            fileContents.append( scanner.nextLine() + lineSeparator );
	        return fileContents.toString();
	    } finally {
	        scanner.close();
	    }
	}
	
	public static void clearConsole() {
		final String operatingSystem = System.getProperty( "os.name" );

		try {
			if( operatingSystem .contains( "Windows" ) )
				Runtime.getRuntime().exec( "cls" );
			else 
			    Runtime.getRuntime().exec( "clear" );
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Tries to get a number input in a range, it can exclude some numbers
	 * @param sc The scanner to read the values
	 * @param length The range
	 * @param exlude An array of exclusions
	 * @return The correct input number 
	 **/
	public static int inputWithRange( Scanner sc, int length, Integer[] exclude ) {
		int number;
		do {
            while( !sc.hasNextInt() ) {
                System.out.println( "That's not an option!" );
                sc.next(); 
            }
            number = Integer.parseInt( sc.nextLine() );
            // If the number is outside range print an error message.
            if( number < 0 || number >= length || Arrays.binarySearch( exclude, number ) >= 0 )
                System.out.println( "Input doesn't match specifications. Try again." );
        } while( number < 0 || number >= length || Arrays.binarySearch( exclude, number ) >= 0 );
		return number;
	}
	
	/** Tries to get a number input in a range
	 * @param sc The scanner to read the values
	 * @param length The range
	 * @return The correct input number 
	 **/
	public static int inputWithRange( Scanner sc, int length ) {
		return inputWithRange( sc, length, new Integer[]{} );
	}
}
