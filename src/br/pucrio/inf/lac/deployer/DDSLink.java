package br.pucrio.inf.lac.deployer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.UUID;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import lac.cnclib.sddl.message.ApplicationMessage;
import lac.cnclib.sddl.message.ClientLibProtocol.PayloadSerialization;
import lac.cnclib.sddl.serialization.Serialization;
import lac.cnet.sddl.objects.ApplicationObject;
import lac.cnet.sddl.objects.Message;
import lac.cnet.sddl.objects.PrivateMessage;
import lac.cnet.sddl.udi.core.SddlLayer;
import lac.cnet.sddl.udi.core.UniversalDDSLayerFactory;
import lac.cnet.sddl.udi.core.UniversalDDSLayerFactory.SupportedDDSVendors;
import lac.cnet.sddl.udi.core.listener.UDIDataReaderListener;
import br.pucrio.inf.lac.components.AppUtils;
import br.pucrio.inf.lac.models.MEPAQuery;
import br.pucrio.inf.lac.models.S2PAQuery;
import br.pucrio.inf.lac.models.base.QueryMessage;
import br.pucrio.inf.lac.models.base.QueryMessage.ACTION;
import br.pucrio.inf.lac.models.base.QueryMessage.DATUM;
import br.pucrio.inf.lac.models.base.QueryMessage.ITEM;
import br.pucrio.inf.lac.models.base.QueryMessage.ROUTE;
import br.pucrio.inf.lac.models.base.QueryMessage.SERVICE;

public class DDSLink implements UDIDataReaderListener<ApplicationObject> {
	/** DEBUG */
	private static final String TAG = DDSLink.class.getSimpleName();
	
	private enum RESOURCES {
		LOW    ( "low" ),
		MEDIUM ( "medium" ),
		HIGH   ( "high" );
		
		private String value;
		
		RESOURCES( String value ) {
			value = this.value;
		}
		
		@Override 
		public String toString() {
		    return value;
		}
	}
	
	/** SDDL Elements */
    private Object receiveMessageTopic;
    private Object toMobileNodeTopic;
    private static SddlLayer core;
    
    /** Resources required for a location */
    private static final Map< String, RESOURCES > mResources = new HashMap<>();;
    /** Rules/Events per location (A beacon - MOUUID - represents a location) */
    private static final Map< String, List< MEPAQuery > > mQueries = new HashMap<>();;
    /** Mobile Hubs Data */
    private static final Map<UUID, UUID> mMobileHubs = new HashMap<>();
    
    /** Input reader */
    private static Scanner sc = new Scanner( System.in );
    
    public static void main(String[] args) {
    	System.out.println( "SDDL: starting..." );
    	new DDSLink();
    	System.out.println( "SDDL: successfully started" );
        
    	// Options
    	SERVICE[] services = QueryMessage.SERVICE.values();
    	
    	String result = null;
        do {
        	// Clears the console
        	AppUtils.clearConsole();
        	
        	// Variables to read the configuration of the message
        	QueryMessage mHubQuery = null;
        	UUID nodeDest = null;
        	
        	// List of keys (UUID of the M-Hubs)
        	List<UUID> nodes = new ArrayList<UUID>( mMobileHubs.keySet() );
        	
        	// Destination options to select
        	System.out.println( "\nSelect an option for your message:" );
        	System.out.println( "r: refresh" );
        	System.out.println( "a: all" );
        	for( int i = 0; i < nodes.size(); ++i ) 
        		System.out.println( i + ": " + nodes.get( i ) );
        	
        	// Input the value
        	do {
        		result = sc.nextLine();
        		if( AppUtils.isNumber( result ) ) {
        			int option = Integer.parseInt( result );
        			// If the number is outside range print an error message.
        			if( option < 0 || option >= nodes.size() )
        				System.out.println( "Input doesn't match specifications. Try again." );
        			else {
        				nodeDest = nodes.get( option );
        				break;
        			}
        		}
            } while( !result.equals( "r" ) && !result.equals( "a" ) );

        	if( result.equals( "r" ) )
        		continue;
  			
        	// Select the Mobile Hub Service
        	int number;
        	System.out.println( "\nChoose a service: " );
        	for( int i = 0; i < services.length; i++ ) 
        		System.out.println( i + ": " + services[ i ] );
        	number = AppUtils.inputWithRange( sc, services.length );
        	
        	switch( services[ number ] ) {
				case S2PA:
					mHubQuery = buildS2PAQuery();
				break;
				
				default:
					mHubQuery = buildMEPAQuery();
				break;
			}
        	
        	System.out.println( "\nMessage: " + mHubQuery.toJSON() );
        	
        	ApplicationMessage appMsg = new ApplicationMessage();
		    appMsg.setPayloadType( PayloadSerialization.JSON );
		    appMsg.setContentObject( "[" + mHubQuery.toJSON() + "]" );
		    
		    if( nodeDest == null )
		    	sendBroadcastMSG( appMsg );
		    else
		    	sendUnicastMSG( appMsg, nodeDest );
        	
		    System.out.println( "\nMessage sent! " );
        	System.out.println( "Press enter to continue or 'x' to leave... " );
        	result = sc.nextLine();
		} while( !result.equalsIgnoreCase( "x" ) );
        
        if( sc != null )
        	sc.close();
    }
    
    /**
     * Constructor
     */
    private DDSLink() {
    	// Create a layer and participant
        core = UniversalDDSLayerFactory.getInstance( SupportedDDSVendors.OpenSplice );
        core.createParticipant( UniversalDDSLayerFactory.CNET_DOMAIN );

        // Receive and write topics to domain
        core.createPublisher();
        core.createSubscriber();

        // ClientLib Events
        receiveMessageTopic = core.createTopic( Message.class, Message.class.getSimpleName() );
        core.createDataReader( this, receiveMessageTopic );

        // To ClientLib
        toMobileNodeTopic = core.createTopic( PrivateMessage.class, PrivateMessage.class.getSimpleName() );
        core.createDataWriter( toMobileNodeTopic );
    }
	
    private static QueryMessage buildMEPAQuery() {
    	MEPAQuery query = new MEPAQuery();
    	int number;
    	
    	// Options
    	ACTION[] actions = QueryMessage.ACTION.values();
    	ITEM[]   items   = QueryMessage.ITEM.values();
    	ROUTE[]  routes  = QueryMessage.ROUTE.values();
    	DATUM[]  data    = QueryMessage.DATUM.values();
    	
    	// Select an operation
    	System.out.println( "\nChoose the query item: " );
    	for( int i = 0; i < items.length; i++ ) 
    		System.out.println( i + ": " + items[ i ] );
    	number = AppUtils.inputWithRange( sc, items.length );
    	
    	switch( items[number] ) {
    		case RULE:
    			query.setObject( items[number] );
    			
    			// Select the action
    	    	System.out.println( "\nChoose an action: " );
    	    	for( int i = 0; i < actions.length; i++ ) 
    	    		System.out.println( i + ": " + actions[ i ] );
    	    	number = AppUtils.inputWithRange( sc, actions.length );
    	    	
    	    	query.setType( actions[ number ] );
    	    	
    	    	// Check for the type of action to continue
    	    	if( query.getType() != QueryMessage.ACTION.CLEAR &&
    	    		query.getType() != QueryMessage.ACTION.GET ) {
    	        	System.out.println( "\nSet a label: " );
    	        	String label = sc.nextLine();
    	        	
    	        	query.setLabel( label );
    	    	}
    	    	
    	    	if( query.getType() == QueryMessage.ACTION.ADD ) {
            		System.out.println( "\nInsert a CEP Rule: " );
            		String rule = sc.nextLine();
            		
    	        	query.setRule( rule );
    	        	
    	        	System.out.println( "\nChoose the target: " );
    	        	for( int i = 0; i < routes.length; i++ ) 
        	    		System.out.println( i + ": " + routes[ i ] );
        	    	number = AppUtils.inputWithRange( sc, routes.length );
        	    	
        	    	query.setTarget( routes[ number ] );
            	}
    		break;
    		
    		case EVENT:
    			query.setObject( items[number] );
    			
    			// Select the action
    	    	System.out.println( "\nChoose an action: " );
    	    	for( int i = 0; i < actions.length; i++ ) {
    	    		if( !actions[i].equals( ACTION.START ) && !actions[i].equals( ACTION.STOP ) )
    	    			System.out.println( i + ": " + actions[ i ] );
    	    	}
    	    	number = AppUtils.inputWithRange( sc, actions.length, new Integer[]{ ACTION.START.ordinal(), ACTION.STOP.ordinal() } );
    	    	
    	    	query.setType( actions[ number ] );
    	    	
    	    	// Check for the type of action to continue
    	    	if( query.getType() != QueryMessage.ACTION.CLEAR &&
    	    		query.getType() != QueryMessage.ACTION.GET ) {
    	        	System.out.println( "\nSet a label: " );
    	        	String label = sc.nextLine();
    	        	
    	        	query.setLabel( label );
    	    	}
    	    	
    	    	if( query.getType() == QueryMessage.ACTION.ADD ) {
    	    		System.out.println( "\nInsert a variable name (0 to cancel): " );
            	}
        	break;
        		
        	// Create a location
    		default:
    			// Options
    			RESOURCES[] resources = RESOURCES.values();
    	    	
    			System.out.println( "\nInsert the MOUUID of the beacon: " );
    			String mouuid = sc.nextLine();
    			
    			System.out.println( "\nChoose the resources level: " );
    			for( int i = 0; i < resources.length; i++ ) 
    	    		System.out.println( i + ": " + resources[ i ] );
    			number = AppUtils.inputWithRange( sc, resources.length );
    			
    			mResources.put( mouuid, resources[number] );
    			mQueries.put( mouuid, new ArrayList<MEPAQuery>() );
    			
    			return null;
    	}
    	
    	return query;
    }
    
    private static QueryMessage buildS2PAQuery() {
    	S2PAQuery query = new S2PAQuery();
    	return query;
    }
    
    /**
     * Sends a message to all the components (Broadcast)
     * @param appMSG The application message (e.g. a String message)
     */
    public static void sendBroadcastMSG( ApplicationMessage appMSG ) {
		PrivateMessage privateMSG = new PrivateMessage();
		privateMSG.setGatewayId( UniversalDDSLayerFactory.BROADCAST_ID );
		privateMSG.setNodeId( UniversalDDSLayerFactory.BROADCAST_ID );
		privateMSG.setMessage( Serialization.toProtocolMessage( appMSG ) );
		
		sendCoreMSG( privateMSG );
    }
    
    /**
     * Sends a message to a unique component (Unicast)
     * @param appMSG The application message (e.g. a String message)
     * @param nodeID The UUID of the receiver
     */
    public static void sendUnicastMSG( ApplicationMessage appMSG, UUID nodeID ) {
		PrivateMessage privateMSG = new PrivateMessage();
		privateMSG.setGatewayId( UniversalDDSLayerFactory.BROADCAST_ID );
		privateMSG.setNodeId( nodeID );
		privateMSG.setMessage( Serialization.toProtocolMessage( appMSG ) );
		
		sendCoreMSG( privateMSG );
    }
    
    /**
     * Writes the message (send)
     * @param privateMSG The message
     */
    private static void sendCoreMSG(PrivateMessage privateMSG) {
        core.writeTopic( PrivateMessage.class.getSimpleName(), privateMSG );
    }
    
    @Override
	public void onNewData(ApplicationObject topicSample) {
		Message msg = null;
		
		if( topicSample instanceof Message ) {
			msg = (Message) topicSample;
			UUID nodeId = msg.getSenderId();
			UUID gatewayId = msg.getGatewayId();
			
			if( !mMobileHubs.containsKey( nodeId ) ){
				mMobileHubs.put( nodeId, gatewayId );
				AppUtils.Logger( TAG, "Client connected" );
			}
			
			JSONParser parser = new JSONParser();
			String content = new String( msg.getContent() );
			try {
	        	JSONObject data = (JSONObject) parser.parse( content );
	        	String tag = (String) data.get( "tag" );
	        	
	        	if( tag.equals( "ErrorData" ) ) {
	        		String uuid      = (String) data.get( "uuid" );
	        		String component = (String) data.get( "component" );
	        		String message   = (String) data.get( "message" );
	        		
	        		AppUtils.Logger( TAG, 
	        				"ErrorData [uuid=" + uuid
				                + ", component=" + component
				                + ", message=" + message
				                + "]");
	        	}
	        	
	        } catch (Exception ex) {}
		}
	}
}

